<?php

namespace TylerKing\MediaScraper;

use Guzzle\Http\Client;
use Guzzle\Http\Message\Response;
use Guzzle\Http\Exception\CurlException;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Exception;

abstract class ApiScraper
{
    protected $url,
              $api_endpoint,
              $api_endpoint_secure,
              $client,
              $response,
              $type,
              $secure;

    public function __construct()
    {
        $this->api_endpoint        = null;
        $this->api_endpoint_secure = null;
        $this->url                 = null;
        $this->response            = null;
        $this->client              = null;
        $this->type                = null;
        $this->secure              = false;
    }

    public function setSecure($secure = true)
    {
        $this->secure = (bool) $secure;

        return $this;
    }

    public function getSecure()
    {
        return (bool) $this->secure;
    }

    public function isSecure()
    {
        return $this->getSecure() === true;
    }

    public function setUrl($url)
    {
        $this->url = trim($url);

        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setClient(Client $client)
    {
        $this->client = $client;

        return $this;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setResponse(Response $response)
    {
        $this->response = $response;

        return $this;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function fetch($retries = 1)
    {
        return $this->_fetch($this->getUrl(), $retries);
    }

    protected function _fetch($url, $retries = 1)
    {
        $this->setClient(new Client($this->isSecure() ? $this->api_endpoint_secure : $this->api_endpoint));

        $tries    = 0;
        $request  = null;
        $response = null;

        do {
            try {
                $request  = $this->getClient()->get($url);
                $response = $request->send();
            } catch (Exception $e) {
                if ($e instanceof ClientErrorResponseException || $e instanceof CurlException) {
                    $request  = null;
                    $response = null;
                }
            }

            if (null !== $response) {
                $this->setResponse($response);
                break;
            }

            $tries++;
        } while ($tries < $retries);

        if (null === $response || null === $request) {
            return false;
        }

        $this->setType($this->getResponse()->getHeader('Content-Type', true));

        return true;
    }
}
