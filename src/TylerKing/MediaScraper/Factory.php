<?php

namespace TylerKing\MediaScraper;

use TylerKing\MediaScraper\VideoInterface as Video;
use TylerKing\MediaScraper\ArticleInterface as Article;
use TylerKing\MediaScraper\ImageInterface as Image;
use TylerKing\MediaScraper\AudioInterface as Audio;
use TylerKing\MediaScraper\ScraperException;
use DirectoryIterator;

class Factory
{
    public function create($url, $class = null)
    {
        if (null === $url) {
            throw new ScraperException('A URL must be passed to the factory method.');
        }

        $url = trim($url);
        if (false === strpos($url, 'http://') && false === strpos($url, 'https://')) {
            $url = 'http://'.$url;
        }

        $tld_position = strrpos($url, '.');
        if (! $tld_position) {
            throw new ScraperException(sprintf('A valid URL must be passed to the factory method. "%s" was passed.', $url));
        }

        $tld = substr($url, $tld_position + 1);
        if (strlen($tld) === 0) {
            throw new ScraperException(sprintf('A valid URL must be passed to the factory method. "%s" was passed.', $url));
        }

        if(! filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_SCHEME_REQUIRED)) {
            throw new ScraperException(sprintf('A valid URL must be passed to the factory method. "%s" was passed.', $url));
        }

        if (null !== $class) {
            $class = new $class;
            if (! $class instanceof Article && ! $class instanceof Video && ! $class instanceof Image && ! $class instanceof Audio) {
                throw new ScraperException(sprintf('"%s" scraper class must use a media interface.', get_class($class)));
            }

            $class->setUrl($url);
            return $class;
        }

        $host = str_replace('www.', '', parse_url($url, PHP_URL_HOST));
        foreach (new DirectoryIterator(__DIR__.'/Scraper') as $file) {
            if ($file->isDot()) {
                continue;
            }

            $scraper_class = 'TylerKing\\MediaScraper\\Scraper\\'.$file->getBasename('.php');
            $class         = new $scraper_class;
            if (! $class instanceof Article && ! $class instanceof Video && ! $class instanceof Image && ! $class instanceof Audio) {
                throw new ScraperException(sprintf('"%s" scraper class must use a media interface.', get_class($class)));
            }

            if (! in_array($host, $class->getDomains())) {
                continue;
            }

            $class->setUrl($url);
            return $class;
        }

        $general_class = 'TylerKing\\MediaScraper\\Scraper\\General';
        $class         = new $general_class;
        $class->setUrl($url);

        return $class;
    }
}
