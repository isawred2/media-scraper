<?php

namespace TylerKing\MediaScraper\Scraper;

use TylerKing\MediaScraper\ApiScraper;
use TylerKing\MediaScraper\VideoInterface as Video;

class Vimeo extends ApiScraper implements Video
{
    public function __construct()
    {
        parent::__construct();

        $this->api_endpoint        = 'http://vimeo.com';
        $this->api_endpoint_secure = 'https://vimeo.com';
        $this->id                  = null;
        $this->title               = null;
        $this->description         = null;
        $this->image               = null;
    }

    public function getProvider()
    {
        return 'Vimeo';
    }

    public function getDomains()
    {
        return ['vimeo.com'];
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId()
    {
        if (null === $this->id) {
            $regexstr = '~
             # Match Vimeo link and embed code
            (?:<iframe [^>]*src=")?         # If iframe match up to first quote of src
            (?:                             # Group vimeo url
                    https?:\/\/             # Either http or https
                    (?:[\w]+\.)*            # Optional subdomains
                    vimeo\.com              # Match vimeo.com
                    (?:[\/\w]*\/videos?)?   # Optional video sub directory this handl$
                    \/                      # Slash before Id
                    ([0-9]+)                # $1: VIDEO_ID is numeric
                    [^\s]*                  # Not a space
            )                               # End group
            "?                              # Match end quote if part of src
            (?:[^>]*></iframe>)?            # Match the end of the iframe
            (?:<p>.*</p>)?                  # Match any title information stuff
            ~ix';
            preg_match($regexstr, $this->getUrl(), $matches);

            if (isset($matches[1])) {
                $this->setId((int) $matches[1]);
            } else {
                $this->setId(null);
            }
        }

        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        if (null === $this->title) {
            if ($this->getResponse()) {
                $json = $this->getResponse()->json()[0];
                $this->setTitle($json['title']);
            } else {
                $this->setTitle(null);
            }
        }

        return $this->title;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        if (null === $this->description) {
            if ($this->getResponse()) {
                $json = $this->getResponse()->json()[0];
                $this->setDescription($json['description']);
            } else {
                $this->setDescription(null);
            }
        }

        return $this->description;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    public function getImage($size = 'large')
    {
        if (null === $this->image) {
            if ($this->getResponse()) {
                $json = $this->getResponse()->json()[0];
                $this->setImage($json['thumbnail_'.$size]);
            } else {
                $this->setImage(null);
            }
        }

        return $this->image;
    }

    public function getEmbedCode($width = null, $height = null)
    {
        $width  = $width ?: 700;
        $height = $height ?: 385;
        $video  = $this->getId();
        $secure = $this->isSecure() ? 's' : '';

        return '<iframe src="http'.$secure.'://player.vimeo.com/video/'.$video.'" width="'.$width.'" height="'.$height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
    }

    public function fetch($retries = 1)
    {
        $url = '/api/v2/video/'.$this->getId().'.json';
        return $this->_fetch($url, $retries);
    }
}
