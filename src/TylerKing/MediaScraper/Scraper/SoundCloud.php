<?php

namespace TylerKing\MediaScraper\Scraper;

use TylerKing\MediaScraper\ApiScraper;
use TylerKing\MediaScraper\AudioInterface as Audio;

class SoundCloud extends ApiScraper implements Audio
{
    public function __construct()
    {
        parent::__construct();

        $this->api_endpoint        = 'https://api.soundcloud.com';
        $this->api_endpoint_secure = $this->api_endpoint;
        $this->id                  = null;
        $this->title               = null;
        $this->description         = null;
        $this->image               = null;
        $this->client_id           = null;
    }

    public function getProvider()
    {
        return 'SoundCloud';
    }

    public function getDomains()
    {
        return ['soundcloud.com'];
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId()
    {
        if (null === $this->id) {
            if ($this->getResponse()) {
                $json = $this->getResponse()->json();
                $this->setId($json['id']);
            } else {
                $this->setId(null);
            }
        }

        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        if (null === $this->title) {
            if ($this->getResponse()) {
                $json = $this->getResponse()->json();
                $this->setTitle($json['title']);
            } else {
                $this->setTitle(null);
            }
        }

        return $this->title;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        if (null === $this->description) {
            if ($this->getResponse()) {
                $json = $this->getResponse()->json();
                $this->setDescription($json['description']);
            } else {
                $this->setDescription(null);
            }
        }

        return $this->description;
    }

    public function setClientId($client_id)
    {
        $this->client_id = $client_id;

        return $this;
    }

    public function getClientId()
    {
        return $this->client_id;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    public function getImage()
    {
        if (null === $this->image) {
            if ($this->getResponse()) {
                $json = $this->getResponse()->json();

                if ($json['artwork_url']) {
                    $art = $json['artwork_url'];
                } else {
                    $art = $json['user']['avatar_url'];
                }

                $this->setImage(str_replace('-large', '-t500x500', $art));
            } else {
                $this->setImage(null);
            }
        }

        return $this->image;
    }

    public function getEmbedCode($width = null, $height = null)
    {
        $width  = $width ?: 700;
        $height = $height ?: 385;
        $track  = $this->getId();
        $secure = $this->isSecure() ? 's' : '';

        return '<iframe width="'.$width.'" height="'.$height.'" scrolling="no" frameborder="no" src="http'.$secure.'://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F'.$track.'"></iframe>';
    }

    public function fetch($retries = 1)
    {
        $url = '/resolve.json?url='.$this->getUrl().'&client_id='.$this->getClientId();
        return $this->_fetch($url, $retries);
    }
}
