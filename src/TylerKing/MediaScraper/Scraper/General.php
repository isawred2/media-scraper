<?php

namespace TylerKing\MediaScraper\Scraper;

use TylerKing\MediaScraper\WebScraper;
use TylerKing\MediaScraper\ArticleInterface as Article;

class General extends WebScraper implements Article
{
    protected $title,
              $image,
              $description;

    public function __construct()
    {
        parent::__construct();

        $this->title       = null;
        $this->image       = null;
        $this->description = null;
    }

    public function getProvider()
    {
        return str_replace('www.', '', parse_url($this->getUrl(), PHP_URL_HOST));
    }

    public function getDomains()
    {
        return [];
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        if (null !== $this->title) {
            return $this->title;
        }

        if ($this->isImage()) {
            $this->setTitle($this->getUrl());
            return $this->title;
        }

        $og_title = $this->crawler->filterXPath('//meta[@property="og:title"]');
        if ($og_title->count() !== 0) {
            $this->setTitle($og_title->attr('content'));
            return $this->title;
        }

        $title = $this->crawler->filterXPath('//title');
        if ($title->count() !== 0) {
            $this->setTitle($title->eq(0)->text());
            return $this->title;
        }

        return null;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    public function getImage()
    {
        if (null !== $this->image) {
            return $this->image;
        }

        if ($this->isImage()) {
            $this->setImage($this->getUrl());
            return $this->image;
        }

        $max_area = 0;
        $max_url  = null;

        $og_image = $this->crawler->filterXPath('//meta[@property="og:image"]');
        if ($og_image->count() !== 0) {
            $this->setImage($og_image->attr('content'));
            return $this->image;
        }

        $images = $this->crawler->filterXPath('//img');
        foreach ($images as $node) {
            $src = $node->getAttribute('src');
            if ($this->_isRelativeUrl($src)) {
                continue;
            }

            $img  = @getimagesize($src);
            if (! isset($img[0])) {
                continue;
            }
            $size = [$img[0], $img[1]];
            $area = $size[0] * $size[1];

            if ($area < 5000) {
                continue;
            }

            if (max($size) / min($size) > 1.5) {
                continue;
            }

            if (strstr($src, 'sprite')) {
                $area /= 10;
            }

            if ($area > $max_area) {
                $max_area = $area;
                $max_url  = $src;
            }
        }

        $this->setImage($max_url);
        return $this->image;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        if (null !== $this->description) {
            return $this->description;
        }

        if ($this->isImage()) {
            $this->setDescription(null);
            return $this->description;
        }

        $og_description = $this->crawler->filterXPath('//meta[@property="og:description"]');
        if ($og_description->count() !== 0) {
            $this->setDescription($og_description->attr('content'));
            return $this->description;
        }

        $description = $this->crawler->filterXPath('//meta[@name="description"]');
        if ($description->count() !== 0) {
            $this->setDescription($description->attr('content'));
            return $this->description;
        }

        $this->setDescription(null);
        return $this->description;
    }
}
