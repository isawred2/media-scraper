<?php

namespace TylerKing\MediaScraper\Scraper;

use TylerKing\MediaScraper\ApiScraper;
use TylerKing\MediaScraper\VideoInterface as Video;

class Youtube extends ApiScraper implements Video
{
    public function __construct()
    {
        parent::__construct();

        $this->api_endpoint        = 'http://gdata.youtube.com';
        $this->api_endpoint_secure = 'https://gdata.youtube.com';
        $this->id                  = null;
        $this->title               = null;
        $this->description         = null;
        $this->image               = null;
    }

    public function getProvider()
    {
        return 'Youtube';
    }

    public function getDomains()
    {
        return ['youtube.com', 'youtu.be'];
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId()
    {
        if (null === $this->id) {
            if (stristr($this->getUrl(), 'youtu.be')) {
                $this->setId(basename($this->getUrl()));
            } else {
                parse_str(parse_url($this->getUrl(), PHP_URL_QUERY), $array);

                if (! isset($array['v'])) {
                    $this->setId(null);
                } else {
                    $this->setId($array['v']);
                }
            }
        }

        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        if (null === $this->title) {
            $json = $this->getResponse()->json();

            if (! isset($json['data']['title'])) {
                $this->setTitle(null);
            } else {
                $this->setTitle($json['data']['title']);
            }
        }

        return $this->title;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        if (null === $this->description) {
            $json = $this->getResponse()->json();

            if (! isset($json['data']['description'])) {
                $this->setDescription(null);
            } else {
                $this->setDescription($json['data']['description']);
            }
        }

        return $this->description;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    public function getImage()
    {
        if (null === $this->image) {
            $json = $this->getResponse()->json();

            if (! isset($json['data']['thumbnail']['hqDefault'])) {
                $this->setImage(null);
            } else {
                $image = $json['data']['thumbnail']['hqDefault'];
                if ($this->isSecure()) {
                    $image = str_replace('http://', 'https://', $image);
                }

                $this->setImage($image);
            }
        }

        return $this->image;
    }

    public function getEmbedCode($width = null, $height = null)
    {
        $width  = $width ?: 700;
        $height = $height ?: 385;
        $video  = $this->getId();
        $secure = $this->isSecure() ? 's' : '';

        return '<iframe class="youtube-player" type="text/html" width="'.$width.'" height="'.$height.'" src="http'.$secure.'://www.youtube.com/embed/'.$video.'?autoplay=0"
                frameborder="0"></iframe>';
    }

    public function fetch($retries = 1)
    {
        $url = "/feeds/api/videos/{$this->getId()}?v=2&alt=jsonc";
        return $this->_fetch($url, $retries);
    }
}
