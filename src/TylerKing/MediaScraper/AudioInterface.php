<?php

namespace TylerKing\MediaScraper;

interface AudioInterface
{
    function getProvider();
    function getDomains();

    function setId($id);
    function getId();

    function setTitle($title);
    function getTitle();

    function setDescription($description);
    function getDescription();

    function getEmbedCode($width, $height);
}
