<?php

namespace TylerKing\MediaScraper;

interface ArticleInterface
{
    function getProvider();
    function getDomains();

    function setTitle($title);
    function getTitle();

    function setDescription($description);
    function getDescription();

    function setImage($image);
    function getImage();
}
