<?php

namespace TylerKing\MediaScraper;

use Guzzle\Http\Client;
use Guzzle\Http\Message\Response;
use Symfony\Component\DomCrawler\Crawler;
use Guzzle\Http\Exception\CurlException;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Exception;

abstract class WebScraper
{
    protected $url,
              $client,
              $crawler,
              $response,
              $type;

    public function __construct()
    {
        $this->url       = null;
        $this->crawler   = null;
        $this->response  = null;
        $this->client    = null;
        $this->type      = null;
    }

    public function setUrl($url)
    {
        $this->url = trim($url);

        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setClient(Client $client)
    {
        $this->client = $client;

        return $this;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setCrawler(Crawler $crawler)
    {
        $this->crawler = $crawler;

        return $this;
    }

    public function getCrawler()
    {
        return $this->crawler;
    }

    public function setResponse(Response $response)
    {
        $this->response = $response;

        return $this;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function isImage()
    {
        return strstr($this->getType(), 'image/') ? true : false;
    }

    public function isArticle()
    {
        return strstr($this->getType(), 'text/html') ? true : false;
    }

    public function fetch()
    {
        return $this->_fetch($this->getUrl());
    }

    protected function _fetch($url, $retries = 1)
    {
        $this->setClient(new Client($url));

        $tries    = 0;
        $request  = null;
        $response = null;

        do {
            try {
                $request  = $this->getClient()->get();
                $response = $request->send();
            } catch (Exception $e) {
                if ($e instanceof ClientErrorResponseException || $e instanceof CurlException) {
                    $request  = null;
                    $response = null;
                }
            }

            if (null !== $response) {
                $this->setResponse($response);
                break;
            }

            $tries++;
        } while ($tries < $retries);

        if (null === $response || null === $request) {
            return false;
        }

        $this->setType($this->getResponse()->getHeader('Content-Type', true));
        $this->setCrawler(new Crawler($this->getResponse()->getBody(true)));

        return true;
    }

    protected function _isRelativeUrl($url)
    {
        return (substr($url, 0, 1) == '/' || substr($url, 0, 1) == '.');
    }
}
