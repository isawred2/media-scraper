<?php

namespace TylerKing\MediaScraper;

interface VideoInterface
{
    function getProvider();
    function getDomains();

    function setId($id);
    function getId();

    function setTitle($title);
    function getTitle();

    function setDescription($description);
    function getDescription();

    function setImage($image);
    function getImage();

    function getEmbedCode($width, $height);
}
