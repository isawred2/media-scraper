<?php

namespace TylerKing\MediaScraper;

use Exception;
use TylerKing\MediaScraper\Factory;
use TylerKing\MediaScraper\Scraper\SoundCloud;

class SoundCloudScraperTest extends \PHPUnit_Framework_TestCase
{
    private $client_id = '1e8e74fe34eba05fa72a821e6c53c678';

    /**
     * @test
     */
    function soundSetup()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://soundcloud.com/matas/hobnotropic');

        $this->assertEquals(true, is_subclass_of($scraper, 'TylerKing\\MediaScraper\\AudioInterface'));
        $this->assertEquals(true, is_subclass_of($scraper, 'TylerKing\\MediaScraper\\ApiScraper'));
    }

    /**
     * @test
     */
    function soundShouldPassUrl()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://soundcloud.com/matas/hobnotropic');

        $this->assertEquals('http://soundcloud.com/matas/hobnotropic', $scraper->getUrl());
    }

    /**
     * @test
     */
    function soundShouldReturnNullDataForNonExistantAudio()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://soundcloud.com/xxxxxxxxxx/hobnotropic');
        $scraper->setClientId($this->client_id);
        $fetch = $scraper->fetch();

        $this->assertEquals(false, $fetch);
        $this->assertEquals(null, $scraper->getTitle());
        $this->assertEquals(null, $scraper->getId());
        $this->assertEquals(null, $scraper->getDescription());
        $this->assertEquals(null, $scraper->getImage());
    }

    /**
     * @test
     */
    function soundEnsureResponseIsPassed()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://soundcloud.com/matas/hobnotropic');
        $scraper->setClientId($this->client_id);
        $scraper->fetch();

        $this->assertInstanceOf('Guzzle\\Http\\Message\\Response', $scraper->getResponse());
    }

    /**
     * @test
     */
    function soundProviderIsSoundCloud()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://soundcloud.com/matas/hobnotropic');

        $this->assertEquals('SoundCloud', $scraper->getProvider());
    }

    /**
     * @test
     */
    function soundDataTest()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://soundcloud.com/matas/hobnotropic');
        $scraper->setClientId($this->client_id);
        $scraper->fetch();

        $this->assertContains('json', $scraper->getType());
        $this->assertContains('json', $scraper->getType());
        $this->assertContains('Hobno', $scraper->getTitle());
        $this->assertContains('Hobno', $scraper->getTitle());
        $this->assertContains('Kinda', $scraper->getDescription());
        $this->assertContains('Kinda', $scraper->getDescription());
        $this->assertContains('500', $scraper->getImage());
        $this->assertContains('500', $scraper->getImage());
        $this->assertEquals(49931, $scraper->getId());
        $this->assertEquals(49931, $scraper->getId());
    }

    /**
     * @test
     */
    function soundDataTestNoArtworkProvided()
    {
        $factory = new Factory;
        $scraper = $factory->create('https://soundcloud.com/jamesdore/bring-it-back');
        $scraper->setClientId($this->client_id);
        $scraper->fetch();

        $this->assertContains('500', $scraper->getImage());
        $this->assertContains('500', $scraper->getImage());
    }

    /**
     * @test
     */
    function soundDataTestSecure()
    {
        $factory = new Factory;
        $scraper = $factory->create('https://soundcloud.com/matas/hobnotropic');
        $scraper->setSecure();
        $scraper->fetch();

        $this->assertContains('https://', $scraper->getEmbedCode());
    }

    /**
     * @test
     */
    function soundEmbedTest()
    {
        $factory = new Factory;
        $scraper = $factory->create('https://soundcloud.com/matas/hobnotropic');
        $scraper->setClientId($this->client_id);
        $scraper->fetch();

        $this->assertContains('49931', $scraper->getEmbedCode());
    }

}
