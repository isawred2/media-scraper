<?php

namespace TylerKing\MediaScraper;

use Exception;
use TylerKing\MediaScraper\Factory;
use TylerKing\MediaScraper\Scraper\General;
use Symfony\Component\DomCrawler\Crawler;

class GeneralScraperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    function generalSetup()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://apple.com');

        $this->assertEquals(true, is_subclass_of($scraper, 'TylerKing\\MediaScraper\\ArticleInterface'));
        $this->assertEquals(true, is_subclass_of($scraper, 'TylerKing\\MediaScraper\\WebScraper'));
    }

    /**
     * @test
     */
    function generalEnsureCrawlerInstanceIsPassed()
    {
        $crawler = new Crawler('<html><body></body></html>');

        $factory = new Factory;
        $scraper = $factory->create('http://apple.com');
        $scraper->setCrawler($crawler);

        $this->assertEquals($crawler, $scraper->getCrawler());
    }

    /**
     * @test
     */
    function generalShouldReturnFalseForBadRequest()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://non-url-here-there-everywhere.com');
        $fetched = $scraper->fetch();

        $this->assertEquals(false, $fetched);
    }

    /**
     * @test
     */
    function generalShouldProvideDomainAsProviderName_TestOne()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://www.apple.com');

        $this->assertEquals('apple.com', $scraper->getProvider());
    }

    /**
     * @test
     */
    function generalShouldProvideDomainAsProviderName_TestTwo()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://apple.com');

        $this->assertEquals('apple.com', $scraper->getProvider());
    }

    /**
     * @test
     */
    function generalShouldReturnNoDomains()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://apple.com');

        $this->assertEquals([], $scraper->getDomains());
    }

    /**
     * @test
     */
    function generalDataScrapeTest()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://apple.com');
        $scraper->fetch();

        $this->assertNotNull($scraper->getTitle());
        $this->assertNotNull($scraper->getImage());

        // Covers re-calling.
        $this->assertNotNull($scraper->getTitle());
        $this->assertNotNull($scraper->getImage());

        // And type.
        $this->assertEquals(true, $scraper->isArticle());
        $this->assertEquals(false, $scraper->isImage());
        $this->assertContains('text/html', $scraper->getType());
    }

    /**
     * @test
     */
    function generalImageOnlyScrapeTest()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://i.imgur.com/UX9ty.jpg');
        $scraper->fetch();

        $this->assertEquals('http://i.imgur.com/UX9ty.jpg', $scraper->getTitle());
        $this->assertEquals('http://i.imgur.com/UX9ty.jpg', $scraper->getImage());
        $this->assertEquals(null, $scraper->getDescription());

        $this->assertEquals(false, $scraper->isArticle());
        $this->assertEquals(true, $scraper->isImage());
        $this->assertContains('image/', $scraper->getType());
    }

    /**
     * @test
     */
    function generalImageOnlyTestSprite()
    {
        $crawler = new Crawler('<html><body><img src="http://www.php.net/images/logos/php-med-trans.png" /><img src="http://laptrinhs2.com/Images/Items/css-sprite-example.png_22_11_2012_17_24_38.png" /><img src="http://i.imgur.com/1GJfNK9.png" /></body></html>');

        $factory = new Factory;
        $scraper = $factory->create('http://apple.com');
        $scraper->fetch();
        $scraper->setCrawler($crawler);

        $this->assertEquals($scraper->getImage(), 'http://i.imgur.com/1GJfNK9.png');
    }

    /**
     * @test
     */
    function generalOgMetaTest()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://ogp.me');
        $scraper->fetch();

        $this->assertContains('Open Graph', $scraper->getTitle());
        $this->assertContains('Open Graph', $scraper->getDescription());
        $this->assertEquals('http://ogp.me/logo.png', $scraper->getImage());
    }

    /**
     * @test
     */
    function generalTitleReturnNullForNoTitle()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://apple.com');
        $scraper->setCrawler(new Crawler('<html><body></body></html>'));

        $this->assertEquals(null, $scraper->getTitle());
    }

    /**
     * @test
     */
    function generalDescriptionTest()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://ogp.me');
        $scraper->fetch();

        $this->assertContains('Open Graph', $scraper->getDescription());

        // Covers a re-run
        $this->assertContains('Open Graph', $scraper->getDescription());

        $scraper = $factory->create('http://apple.com');
        $scraper->setCrawler(new Crawler('<html><head><meta name="description" content="test" /></head><body></body></html>'));

        $this->assertEquals('test', $scraper->getDescription());
    }

    /**
     * @test
     */
    function generalTitleReturnNullForNoDescription()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://apple.com');
        $scraper->setCrawler(new Crawler('<html><body></body></html>'));

        $this->assertEquals(null, $scraper->getDescription());
    }

    /**
     * @test
     */
    function generalShouldReturnNoImagesForAllRelative()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://apple.com');
        $scraper->setCrawler(new Crawler('<html><body><img src="./test.jpg" /><img src="./test2.jpg" /></body></html>'));

        $this->assertEquals(null, $scraper->getImage());
    }

    /**
     * @test
     */
    function generalShouldReturnSkipImagesWithSpriteName()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://apple.com');
        $scraper->setCrawler(new Crawler('<html><body><img src="./test.jpg" /><img src="http://www.examples.com/imgs/icon_sprite.jpg" /></body></html>'));

        $this->assertEquals(null, $scraper->getImage());
    }
}
