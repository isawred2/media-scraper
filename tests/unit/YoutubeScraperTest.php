<?php

namespace TylerKing\MediaScraper;

use Exception;
use TylerKing\MediaScraper\Factory;
use TylerKing\MediaScraper\Scraper\Youtube;

class YoutubeScraperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    function youtubeSetup()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://www.youtube.com/watch?v=wpWvkFlyl4M');

        $this->assertEquals(true, is_subclass_of($scraper, 'TylerKing\\MediaScraper\\VideoInterface'));
        $this->assertEquals(true, is_subclass_of($scraper, 'TylerKing\\MediaScraper\\ApiScraper'));
    }

    /**
     * @test
     */
    function youtubeSetupShortUrl()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://youtu.be/PgVbQNOtmxk');

        $this->assertEquals(true, is_subclass_of($scraper, 'TylerKing\\MediaScraper\\VideoInterface'));
        $this->assertEquals(true, is_subclass_of($scraper, 'TylerKing\\MediaScraper\\ApiScraper'));
    }

    /**
     * @test
     */
    function youtubeSetupShortUrlExtracted()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://youtu.be/PgVbQNOtmxk');

        $this->assertEquals(true, is_subclass_of($scraper, 'TylerKing\\MediaScraper\\VideoInterface'));
        $this->assertEquals(true, is_subclass_of($scraper, 'TylerKing\\MediaScraper\\ApiScraper'));
        $this->assertEquals('PgVbQNOtmxk', $scraper->getId());
    }

    /**
     * @test
     */
    function youtubeShouldPassUrl()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://www.youtube.com/watch-wpWvkFlyl4M');

        $this->assertEquals('http://www.youtube.com/watch-wpWvkFlyl4M', $scraper->getUrl());
    }

    /**
     * @test
     */
    function youtubeShouldReturnNullDataForNonExistantVideo_One()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://www.youtube.com/watch-wpWvkFlyl4M');
        $scraper->fetch();

        $this->assertEquals(null, $scraper->getId());
        $this->assertEquals(null, $scraper->getTitle());
        $this->assertEquals(null, $scraper->getDescription());
        $this->assertEquals(null, $scraper->getImage());
    }

    /**
     * @test
     */
    function youtubeEnsureResponseIsPassed()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://www.youtube.com/watch?v=wpWvkFlyl4M');
        $scraper->fetch();

        $this->assertInstanceOf('Guzzle\\Http\\Message\\Response', $scraper->getResponse());
    }

    /**
     * @test
     */
    function youtubeProviderIsYoutube()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://www.youtube.com/watch?v=wpWvkFlyl4M');

        $this->assertEquals('Youtube', $scraper->getProvider());
    }

    /**
     * @test
     */
    function youtubeDataTest()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://www.youtube.com/watch?v=wpWvkFlyl4M');
        $scraper->fetch();

        $this->assertContains('json', $scraper->getType());
        $this->assertContains('json', $scraper->getType());
        $this->assertContains('Dark Shadows', $scraper->getTitle());
        $this->assertContains('Dark Shadows', $scraper->getTitle());
        $this->assertContains('Tim Burton', $scraper->getDescription());
        $this->assertContains('Tim Burton', $scraper->getDescription());
        $this->assertNotEquals(null, $scraper->getImage());
        $this->assertNotEquals(null, $scraper->getImage());
    }

    /**
     * @test
     */
    function youtubeEmbedTest()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://www.youtube.com/watch?v=wpWvkFlyl4M');

        $this->assertContains('wpWvkFlyl4M', $scraper->getEmbedCode());
    }

    /**
     * @test
     */
    function youtubeDataTestSecure()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://www.youtube.com/watch?v=wpWvkFlyl4M');
        $scraper->setSecure();
        $scraper->fetch();

        $this->assertContains('https://', $scraper->getImage());
        $this->assertContains('https://', $scraper->getEmbedCode());
    }
}
