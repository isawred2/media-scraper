<?php

namespace TylerKing\MediaScraper;

use Exception;
use TylerKing\MediaScraper\Factory;
use TylerKing\MediaScraper\Scraper\Vimeo;

class VimeoScraperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    function vimeoSetup()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://vimeo.com/7100569');

        $this->assertEquals(true, is_subclass_of($scraper, 'TylerKing\\MediaScraper\\VideoInterface'));
        $this->assertEquals(true, is_subclass_of($scraper, 'TylerKing\\MediaScraper\\ApiScraper'));
    }

    /**
     * @test
     */
    function vimeoShouldPassUrl()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://vimeo.com/7100569');

        $this->assertEquals('http://vimeo.com/7100569', $scraper->getUrl());
    }

    /**
     * @test
     */
    function vimeoShouldReturnNullDataForNonExistantVideo_One()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://vimeo.com/v/303020');
        $scraper->fetch();

        $this->assertEquals(null, $scraper->getTitle());
        $this->assertEquals(null, $scraper->getDescription());
        $this->assertEquals(null, $scraper->getImage());
    }

    /**
     * @test
     */
    function vimeoEnsureResponseIsPassed()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://vimeo.com/7100569');
        $scraper->fetch();

        $this->assertInstanceOf('Guzzle\\Http\\Message\\Response', $scraper->getResponse());
    }

    /**
     * @test
     */
    function vimeoProviderIsVimeo()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://vimeo.com/7100569');

        $this->assertEquals('Vimeo', $scraper->getProvider());
    }

    /**
     * @test
     */
    function vimeoDataTest()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://vimeo.com/7100569');
        $scraper->fetch();

        $this->assertContains('json', $scraper->getType());
        $this->assertContains('json', $scraper->getType());
        $this->assertContains('Brad', $scraper->getTitle());
        $this->assertContains('Brad', $scraper->getTitle());
        $this->assertContains('Brad', $scraper->getDescription());
        $this->assertContains('Brad', $scraper->getDescription());
        $this->assertNotEquals(null, $scraper->getImage());
        $this->assertNotEquals(null, $scraper->getImage());
    }

    /**
     * @test
     */
    function vimeoEmbedTest()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://vimeo.com/7100569');

        $this->assertContains('7100569', $scraper->getEmbedCode());
    }

    /**
     * @test
     */
    function vimeoDataTestSecure()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://vimeo.com/7100569');
        $scraper->setSecure();
        $scraper->fetch();

        $this->assertContains('https://', $scraper->getImage());
        $this->assertContains('https://', $scraper->getEmbedCode());
    }
}
