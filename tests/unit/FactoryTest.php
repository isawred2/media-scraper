<?php

namespace TylerKing\MediaScraper;

use TylerKing\MediaScraper\ScraperException;
use TylerKing\MediaScraper\Factory;
use TylerKing\MediaScraper\VideoInterface as Video;

class FactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @expectedException TylerKing\MediaScraper\ScraperException
     * @expectedExceptionMessage A URL must be passed to the factory method.
     */
    function factoryShouldThrowExceptionForNullUrl()
    {
        $factory = new Factory;
        $factory->create(null);
    }

    /**
     * @test
     * @expectedException TylerKing\MediaScraper\ScraperException
     * @expectedExceptionMessage A valid URL must be passed to the factory method.
     */
    function factoryShouldThrowExceptionForInvalidUrl_IntTest()
    {
        $factory = new Factory;
        $factory->create(1);
    }

    /**
     * @test
     * @expectedException TylerKing\MediaScraper\ScraperException
     * @expectedExceptionMessage A valid URL must be passed to the factory method.
     */
    function factoryShouldThrowExceptionForInvalidUrl_StrTest()
    {
        $factory = new Factory;
        $factory->create('something');
    }

    /**
     * @test
     * @expectedException TylerKing\MediaScraper\ScraperException
     * @expectedExceptionMessage A valid URL must be passed to the factory method.
     */
    function factoryShouldThrowExceptionForInvalidUrl_PartTest()
    {
        $factory = new Factory;
        $factory->create('://apple.com');
    }

    /**
     * @test
     * @expectedException TylerKing\MediaScraper\ScraperException
     * @expectedExceptionMessage A valid URL must be passed to the factory method.
     */
    function factoryShouldThrowExceptionForInvalidUrl_PartTestTwo()
    {
        $factory = new Factory;
        $factory->create('apple.');
    }

    /**
     * @test
     * @expectedException TylerKing\MediaScraper\ScraperException
     * @expectedExceptionMessage scraper class must use a media interface.
     */
    function factoryShouldThrowExceptionForInvalidPassedClass()
    {
        $factory = new Factory;
        $factory->create('http://apple.com', 'TylerKing\\MediaScraper\\CallableStub');
    }

    /**
     * @test
     */
    function factoryShouldInitAPassedClassWhichHasInterface()
    {
        $factory = new Factory;
        $factory->create('http://apple.com', 'TylerKing\\MediaScraper\\Scraper\\Youtube');
    }

    /**
     * @test
     */
    function factoryShouldPassUrlToPassedClass()
    {
        $url = 'http://apple.com';

        $factory = new Factory;
        $scraper = $factory->create($url, 'TylerKing\\MediaScraper\\Scraper\\Youtube');

        $this->assertEquals($url, $scraper->getUrl());
    }

    /**
     * @test
     */
    function factoryShouldFindAppropiateScraperForUrl_Website()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://apple.com');

        $this->assertEquals('TylerKing\\MediaScraper\\Scraper\\General', get_class($scraper));
    }

    /**
     * @test
     */
    function factoryShouldFindAppropiateScraperForUrl_Youtube()
    {
        $factory = new Factory;
        $scraper = $factory->create('http://www.youtube.com/watch?v=wpWvkFlyl4M');

        $this->assertEquals('TylerKing\\MediaScraper\\Scraper\\Youtube', get_class($scraper));
    }

    /**
     * @test
     */
    function factoryShouldFindAppropiateScraperForUrlAndPassUrl()
    {
        $url = 'http://www.youtube.com/watch?v=wpWvkFlyl4M';

        $factory = new Factory;
        $scraper = $factory->create($url);

        $this->assertEquals($url, $scraper->getUrl());
    }
}

class CallableStub { }